import Controller from '@ember/controller';
import { equal } from '@ember/object/computed';
import config from 'google-interview-homework/config/environment';

export default Controller.extend({
  config,

  isDevelopment: equal('config.environment', 'development')
});
