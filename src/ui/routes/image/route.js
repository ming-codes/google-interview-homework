import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  store: service(),

  model({ id }) {
    return this.get('store').find('image', id);
  }
});
