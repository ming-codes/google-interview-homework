import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  store: service(),

  model() {
    return this.controllerFor(this.routeName).get('model') || this.get('store').findAll('image');
  }
});
