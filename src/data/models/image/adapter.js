import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
  findAll() {
    return {
      data: [ ...new Array(10).keys() ]
        .map(id => {
          return {
            id,
            type: 'image',
            attributes: {
            }
          }
        })
    };
  },

  findRecord(store, type, id) {
    return {
      data: {
        id,
        type: 'image',
        attributes: {
        }
      }
    };
  }
});
