'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    babel: {
      // enable "loose" mode
      loose: true,
      // don't transpile generator functions
      exclude: [ 'transform-regenerator' ],
      plugins: []
    }
  });

  return app.toTree();
};
