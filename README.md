# Requirement Gathering

When given a new requirement, I go over the requirement and list out all the possible states. Any part of the requirement that is data driven should have at least four states: loading, loaded, error, and empty. Lists with infinite scroll feature requires 4 additional states which are the subsequential request loading, loaded, error, and empty state. Make sure all of these states are explicitly defined in the requirement document. Buttons and other interactables have 3 states: hover, focus, active. These needs to be defined in the requirement.

For the photo gallery app, the gallery and image view both have it's own loading, loaded, and error state with the gallery have an empty state. The requirement did not specify a load more feature, so there won't be any subsequential load states. The image view have 6 buttons that each should have hover, focus, and active state.

# Design

The design of a feature starts with the application route, the information architecture. Determine how everything will navigate to and out of the feature. The next step is to componentize the design. I like the atomic design methodology where components are designed from the lowest level and build up to complex feature. Within each component, I employs the use of BEM naming convention to ensure unique class names. I also make uses of BEM naming convention with ids on an application level where each feature area is ensured to have a unique id.

# Implementation and Testing

Tests are always important. Test driven development is a process I use frequently, but not as religiously as some other people. Encapsulated behaviors are often implemented using the TDD approach and so are small components. For complex application level features, I often work on the feature first, then writes the tests.

# Refactor

After implementation and tests are written, it is time to refactor. Certain reusable parts of the screen can be refactored as components, but not the other way around. I am a strong believer that features should be kept architecturally simple and only refactor to components after I have a solid understanding of the requirement.

# Future Considerations

Flipping through of the image gallery can be implemented. I'm not a big fan of gestures as these features have low discoverability. Swiping gestures interferes with iOS's native swipe to go back feature. I would suggest to go with dual paddles on the side for nativation.

Even though the goal of this excercise is to implement the transition from gallery to detail view, it is best to avoid the transition animation all together as they interfere with native transition animations on iOS as you swipe left to go back.
